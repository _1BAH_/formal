package atp.formal.automata.dfa.builders;

import atp.formal.automata.builders.exceptions.AutomatonBuilderException;
import atp.formal.automata.dfa.Cdfa;
import atp.formal.automata.dfa.Dfa;
import atp.formal.automata.nfa.EpsilonNfaWithSingleTerminalState;
import atp.formal.automata.nfa.Nfa;
import atp.formal.automata.nfa.builders.EpsilonNfaBuilder;
import atp.formal.automata.nfa.builders.NfaBuilder;
import atp.formal.parser.InfixParser;
import atp.formal.parser.exceptions.RegexpParserException;
import atp.formal.parser.exceptions.RegexpPreprocessorError;
import atp.formal.syntaxTree.SyntaxTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CdfaBuilderTests {
    @Test
    void buildTest() throws RegexpParserException, RegexpPreprocessorError, AutomatonBuilderException {
        String regexp = "(1|a)b*(a|b+a)";
        String automaton = """
                complete deterministic finite automaton
                                
                Start state:
                000
                                
                Final state:
                101
                101-0011
                101-1111
                                
                δ:
                000 a 101-0011
                000 b 0101-11001
                0101-11001 a 101-1111
                0101-11001 b 0101-11001
                101 a sink
                101 b sink
                101-0011 a 101
                101-0011 b 0101-11001
                101-1111 a sink
                101-1111 b sink
                sink a sink
                sink b sink
                """;

        Set<Character> alphabet = new HashSet<>(List.of('a', 'b'));
        SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
        EpsilonNfaWithSingleTerminalState epsilonNfa = new EpsilonNfaBuilder().setSyntaxTree(tree).build();
        Nfa nfa = new NfaBuilder().setEpsilonNfa(epsilonNfa).build();
        Dfa dfa = new DfaBuilder().setNfa(nfa).build();
        Assertions.assertDoesNotThrow(() -> {
            Cdfa cdfa = new CdfaBuilder().setAlphabet(alphabet).setDfa(dfa).build();
            Assertions.assertEquals(automaton, cdfa.toString());
        });
    }

    @Test
    void trickyTest() throws RegexpParserException, RegexpPreprocessorError, AutomatonBuilderException {
        String regexp = "a*b*";
        String automaton = """
                complete deterministic finite automaton
                                
                Start state:
                00
                                
                Final state:
                00
                001
                101
                                
                δ:
                00 a 001
                00 b 101
                001 a 001
                001 b 101
                101 a sink
                101 b 101
                sink a sink
                sink b sink
                """;

        Set<Character> alphabet = new HashSet<>(List.of('a', 'b'));
        SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
        EpsilonNfaWithSingleTerminalState epsilonNfa = new EpsilonNfaBuilder().setSyntaxTree(tree).build();
        Nfa nfa = new NfaBuilder().setEpsilonNfa(epsilonNfa).build();
        Dfa dfa = new DfaBuilder().setNfa(nfa).build();

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals(automaton, new CdfaBuilder().setAlphabet(alphabet).setDfa(dfa).build().toString());
        });
    }
}
