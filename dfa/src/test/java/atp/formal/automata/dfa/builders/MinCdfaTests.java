package atp.formal.automata.dfa.builders;

import atp.formal.automata.builders.exceptions.AutomatonBuilderException;
import atp.formal.automata.dfa.Cdfa;
import atp.formal.automata.dfa.Dfa;
import atp.formal.automata.nfa.EpsilonNfaWithSingleTerminalState;
import atp.formal.automata.nfa.Nfa;
import atp.formal.automata.nfa.builders.EpsilonNfaBuilder;
import atp.formal.automata.nfa.builders.NfaBuilder;
import atp.formal.parser.InfixParser;
import atp.formal.parser.exceptions.RegexpParserException;
import atp.formal.parser.exceptions.RegexpPreprocessorError;
import atp.formal.syntaxTree.SyntaxTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MinCdfaTests {
    @Test
    void buildTest() throws RegexpParserException, RegexpPreprocessorError, AutomatonBuilderException {
        String regexp = "(1|a)b*(a|b+a)";
        String automaton = """
                minimal complete deterministic finite automaton
                                
                Start state:
                m0
                                
                Final state:
                m2
                m3
                                
                δ:
                m0 a m3
                m0 b m4
                m1 a m1
                m1 b m1
                m2 a m1
                m2 b m1
                m3 a m2
                m3 b m4
                m4 a m2
                m4 b m4
                """;

        Set<Character> alphabet = new HashSet<>(List.of('a', 'b'));
        SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
        EpsilonNfaWithSingleTerminalState epsilonNfa = new EpsilonNfaBuilder().setSyntaxTree(tree).build();
        Nfa nfa = new NfaBuilder().setEpsilonNfa(epsilonNfa).build();
        Dfa dfa = new DfaBuilder().setNfa(nfa).build();
        Cdfa cdfa = new CdfaBuilder().setAlphabet(alphabet).setDfa(dfa).build();
        Assertions.assertDoesNotThrow(() ->{
            Assertions.assertEquals(automaton, new MinCdfaBuilder().setCdfa(cdfa).build().toString());
        });
    }

    @Test
    void trickyTest() throws RegexpParserException, RegexpPreprocessorError, AutomatonBuilderException {
        String regexp = "((a+)(a*))+";
        String automaton = """
                minimal complete deterministic finite automaton
                                
                Start state:
                m0
                                
                Final state:
                m1
                                
                δ:
                m0 a m1
                m1 a m1
                """;

        Set<Character> alphabet = new HashSet<>(List.of('a'));
        SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
        EpsilonNfaWithSingleTerminalState epsilonNfa = new EpsilonNfaBuilder().setSyntaxTree(tree).build();
        Nfa nfa = new NfaBuilder().setEpsilonNfa(epsilonNfa).build();
        Dfa dfa = new DfaBuilder().setNfa(nfa).build();
        Cdfa cdfa = new CdfaBuilder().setAlphabet(alphabet).setDfa(dfa).build();
        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals(automaton, new MinCdfaBuilder().setCdfa(cdfa).build().toString());
        });
    }
}
