package atp.formal.automata.dfa;

import atp.formal.automata.builders.exceptions.AutomatonBuilderException;
import atp.formal.automata.dfa.builders.CdfaBuilder;
import atp.formal.automata.dfa.builders.DfaBuilder;
import atp.formal.automata.nfa.EpsilonNfaWithSingleTerminalState;
import atp.formal.automata.nfa.Nfa;
import atp.formal.automata.nfa.builders.EpsilonNfaBuilder;
import atp.formal.automata.nfa.builders.NfaBuilder;
import atp.formal.automata.states.DfaState;
import atp.formal.automata.states.State;
import atp.formal.parser.InfixParser;
import atp.formal.parser.exceptions.RegexpParserException;
import atp.formal.parser.exceptions.RegexpPreprocessorError;
import atp.formal.syntaxTree.SyntaxTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CdfaTests {
    @Test
    void inversionsTest() throws RegexpParserException, RegexpPreprocessorError, AutomatonBuilderException {
        Set<Character> alphabet = new HashSet<>(List.of('a', 'b', 'c'));
        String regexp = "(1|a)b*(a|b+a)";
        SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
        EpsilonNfaWithSingleTerminalState epsilonNfa = new EpsilonNfaBuilder().setSyntaxTree(tree).build();
        Nfa nfa = new NfaBuilder().setEpsilonNfa(epsilonNfa).build();
        Dfa dfa = new DfaBuilder().setNfa(nfa).build();
        Cdfa cdfa = new CdfaBuilder().setAlphabet(alphabet).setDfa(dfa).build();
        Set<DfaState> before = cdfa.getStates().stream().filter(State::isTerminal).collect(Collectors.toSet());
        Assertions.assertTrue(cdfa.invert().states.stream().filter(State::isTerminal).noneMatch(before::contains));
    }
}
