package atp.formal.automata.dfa.builders;

import atp.formal.automata.builders.exceptions.AutomatonBuilderException;
import atp.formal.automata.nfa.EpsilonNfaWithSingleTerminalState;
import atp.formal.automata.nfa.Nfa;
import atp.formal.automata.nfa.builders.EpsilonNfaBuilder;
import atp.formal.automata.nfa.builders.NfaBuilder;
import atp.formal.parser.InfixParser;
import atp.formal.parser.exceptions.RegexpParserException;
import atp.formal.parser.exceptions.RegexpPreprocessorError;
import atp.formal.syntaxTree.SyntaxTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;

public class DfaBuilderTests {
    @Test
    void buildTest() throws RegexpParserException, RegexpPreprocessorError, AutomatonBuilderException {
        String regexp = "((a+)(a*))+";
        String automaton = """
                deterministic finite automaton
                                
                Start state:
                0
                                
                Final state:
                0001
                0101-0001
                                
                δ:
                0 a 0001
                0001 a 0101-0001
                0101-0001 a 0101-0001
                """;

        SyntaxTree tree = new InfixParser().setAlphabet(new HashSet<>(List.of('a'))).setRegexp(regexp).parse();
        EpsilonNfaWithSingleTerminalState epsilonNfa = new EpsilonNfaBuilder().setSyntaxTree(tree).build();
        Nfa nfa = new NfaBuilder().setEpsilonNfa(epsilonNfa).build();
        Assertions.assertDoesNotThrow(() ->{
            Assertions.assertEquals(automaton, new DfaBuilder().setNfa(nfa).build().toString());
        });
    }

    @Test
    void trickyTest() throws RegexpParserException, RegexpPreprocessorError, AutomatonBuilderException {
        String regexp = "(1|a)b*(a|b+a)";
        String automaton = """
                deterministic finite automaton
                                
                Start state:
                000
                                
                Final state:
                101
                101-0011
                101-1111
                                
                δ:
                000 a 101-0011
                000 b 0101-11001
                0101-11001 a 101-1111
                0101-11001 b 0101-11001
                101-0011 a 101
                101-0011 b 0101-11001
                """;

        SyntaxTree tree = new InfixParser().setAlphabet(new HashSet<>(List.of('a', 'b', 'c'))).setRegexp(regexp).parse();
        EpsilonNfaWithSingleTerminalState epsilonNfa = new EpsilonNfaBuilder().setSyntaxTree(tree).build();
        Nfa nfa = new NfaBuilder().setEpsilonNfa(epsilonNfa).build();
        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals(automaton, new DfaBuilder().setNfa(nfa).build().toString());
        });
    }
}
