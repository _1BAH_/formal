package atp.formal.automata.dfa.builders;

import atp.formal.automata.builders.AutomatonBuilder;
import atp.formal.automata.dfa.Cdfa;
import atp.formal.automata.dfa.Dfa;
import atp.formal.automata.states.DfaState;

import java.util.Set;

public class CdfaBuilder implements AutomatonBuilder {
    private Dfa dfa;
    private Set<Character> alphabet;
    private boolean addSink = false;
    private final DfaState sink;

    public CdfaBuilder() {
        sink = new DfaState();
        sink.setName("sink");
    }

    public CdfaBuilder setDfa(Dfa dfa) {
        this.dfa = dfa;
        return this;
    }

    public CdfaBuilder setAlphabet(Set<Character> alphabet) {
        this.alphabet = alphabet;

        for (char sym: alphabet) {
            sink.addDfaState(sink, sym);
        }

        return this;
    }

    @Override
    public Cdfa build() {
        Cdfa cdfa = new Cdfa();
        cdfa.setAlphabet(alphabet);

        System.out.println(">> Building CDFA from DFA <<");
        System.out.println(dfa);
        System.out.println("<< ---------------------- >>");

        for (DfaState state : dfa.getStates()) {
            for (Character sym : alphabet) {
                if (!state.getΔ().containsKey(sym)) {
                    addSink = true;
                    state.addDfaState(sink, sym);
                }

                cdfa.getStates().add(state);
            }
        }

        if (addSink) {
            cdfa.getStates().add(sink);
        }

        cdfa.setStartState(dfa.getStartState());

        return cdfa;
    }
}
