package atp.formal.automata.dfa.builders;

import atp.formal.automata.builders.AutomatonBuilder;
import atp.formal.automata.states.DfaState;
import atp.formal.automata.states.NfaState;
import atp.formal.automata.dfa.Dfa;
import atp.formal.automata.nfa.Nfa;
import lombok.Setter;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

@Setter
public class DfaBuilder implements AutomatonBuilder {
    private Nfa nfa;

    @Override
    public Dfa build() {
        Dfa dfa = new Dfa(new DfaState().addNfaState(nfa.getStartState()).calculateName());
        Queue<DfaState> queue = new ArrayDeque<>();
        Map<Set<NfaState>, DfaState> dfaStates = new HashMap<>();

        dfaStates.put(dfa.getStartState().getNfaStates(), dfa.getStartState());

        queue.add(dfa.getStartState());

        while (!queue.isEmpty()) {
            DfaState curr = queue.poll();

            for (Map.Entry<Character, Set<NfaState>> entry : curr.getNfaTransitions().entrySet()) {
                Character sym = entry.getKey();
                Set<NfaState> adjacentStates = entry.getValue();
                if (dfaStates.containsKey(adjacentStates)) {
                    curr.addDfaState(dfaStates.get(adjacentStates), sym);
                } else {
                    DfaState state = new DfaState();
                    adjacentStates.forEach(state::addNfaState);
                    dfaStates.put(adjacentStates, state.calculateName());
                    curr.addDfaState(state, sym);
                    queue.add(state);
                }
            }
        }

        return dfa;
    }
}
