package atp.formal.automata.dfa;

import atp.formal.automata.Automaton;
import atp.formal.automata.PreprocessToString;
import atp.formal.automata.states.DfaState;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

@Setter
@Getter
public class Dfa extends Automaton {
    protected DfaState startState;
    protected Set<DfaState> states;
    protected Set<DfaTransition> transitionsTable;

    record DfaTransition(DfaState from, Character sym, DfaState to) {}

    public Dfa() {
        super();
        this.states = new HashSet<>();
    }

    public Dfa(DfaState startState) {
        this();
        this.startState = startState;
    }

    @Override
    protected String getAutomataName() {
        return "deterministic finite automaton";
    }

    @PreprocessToString
    private void collectStates() {
        if (startState == null) {
            return;
        }

        Queue<DfaState> queue = new ArrayDeque<>();
        queue.add(startState);

        while (!queue.isEmpty()) {
            DfaState curr = queue.poll();

            if (!states.contains(curr)) {
                states.add(curr);
                curr.getΔ().forEach((sym, state) -> queue.add(state));
            }

        }
    }

    private Dfa generateTransitionTable() {
        transitionsTable = new HashSet<>();
        states.forEach((from) -> from.getΔ().forEach((sym, to) -> transitionsTable.add(new DfaTransition(from, sym, to))));
        return this;
    }

    private String transitionTableToString() {
        return transitionsTable.stream().map(transition -> new StringBuilder()
                .append(transition.from.getName())
                .append(" ")
                .append(transition.sym)
                .append(" ")
                .append(transition.to.getName())
                .toString()).sorted().collect(Collectors.joining("\n"));
    }

    @Override
    protected String getTerminalStates() {
        StringBuilder builder = new StringBuilder();
        states.stream().filter(DfaState::isTerminal).map(DfaState::getName).sorted().forEach((stateName) -> {
            builder.append(stateName).append("\n");
        });

        return builder.toString();
    }

    @Override
    protected String getTransitionTable() {
        return generateTransitionTable().transitionTableToString();
    }
}
