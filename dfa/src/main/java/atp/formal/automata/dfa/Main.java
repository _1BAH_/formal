package atp.formal.automata.dfa;

import atp.formal.automata.builders.exceptions.AutomatonBuilderException;
import atp.formal.automata.dfa.builders.CdfaBuilder;
import atp.formal.automata.dfa.builders.DfaBuilder;
import atp.formal.automata.dfa.builders.MinCdfaBuilder;
import atp.formal.automata.nfa.EpsilonNfaWithSingleTerminalState;
import atp.formal.automata.nfa.Nfa;
import atp.formal.automata.nfa.builders.EpsilonNfaBuilder;
import atp.formal.automata.nfa.builders.NfaBuilder;
import atp.formal.parser.InfixParser;
import atp.formal.parser.exceptions.RegexpParserException;
import atp.formal.parser.exceptions.RegexpPreprocessorError;
import atp.formal.syntaxTree.SyntaxTree;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws RegexpParserException, RegexpPreprocessorError, AutomatonBuilderException {
        String regexp = "(b(a|b)*|ab*a(a|b)*)";
        Set<Character> alphabet = new HashSet<>(List.of('a', 'b'));
        SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
        EpsilonNfaWithSingleTerminalState epsilonNfa = new EpsilonNfaBuilder().setSyntaxTree(tree).build();
        Nfa nfa = new NfaBuilder().setEpsilonNfa(epsilonNfa).build();
        Dfa dfa = new DfaBuilder().setNfa(nfa).build();
        Cdfa cdfa = new CdfaBuilder().setAlphabet(alphabet).setDfa(dfa).build();
        System.out.println(new MinCdfaBuilder().setCdfa(cdfa).build().toString());
    }
}
