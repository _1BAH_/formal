package atp.formal.automata.dfa.builders;

import atp.formal.automata.builders.AutomatonBuilder;
import atp.formal.automata.dfa.Cdfa;
import atp.formal.automata.states.DfaState;
import atp.formal.automata.states.State;
import atp.formal.automata.dfa.MinCdfa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class MinCdfaBuilder implements AutomatonBuilder {
    private Cdfa cdfa;
    private Set<Set<DfaState>> partitions = new HashSet<>();
    private boolean finished = false;
    private Map<DfaState, Set<DfaState>> stateToGroup = new HashMap<>();

    public MinCdfaBuilder setCdfa(Cdfa cdfa) {
        this.cdfa = cdfa;
        return this;
    }

    private void partition() {
        while (!finished) {
            finished = true;
            Set<Set<DfaState>> newPartitioning = new HashSet<>();
            Map<DfaState, Set<DfaState>> stateToNewGroup = new HashMap<>();

            for (var indistinguishable: partitions) {
                indistinguishable.forEach(state1 -> indistinguishable.forEach(state2 -> {
                    boolean equal = cdfa.getAlphabet().stream()
                            .map(sym -> stateToGroup.get(state1.getΔ().get(sym))
                                    .equals(stateToGroup.get(state2.getΔ().get(sym))))
                            .reduce(true, (acc, curr) -> acc && curr);

                    if (equal) {
                        if (stateToNewGroup.containsKey(state1)) {
                            stateToNewGroup.get(state1).add(state2);

                            if (stateToNewGroup.containsKey(state2)) {
                                stateToNewGroup.get(state2).add(state1);
                            } else {
                                stateToNewGroup.put(state2, stateToNewGroup.get(state1));
                            }
                        } else {
                            if (stateToNewGroup.containsKey(state2)) {
                                stateToNewGroup.get(state2).add(state1);
                                stateToNewGroup.put(state1, stateToNewGroup.get(state2));
                            } else {
                                Set<DfaState> group = new HashSet<>();
                                group.add(state1);
                                group.add(state2);

                                stateToNewGroup.put(state1, group);
                                stateToNewGroup.put(state2, group);
                            }
                        }
                    } else {
                        finished = false;
                        if (!stateToNewGroup.containsKey(state1)) {
                            stateToNewGroup.put(state1, new HashSet<>());
                            stateToNewGroup.get(state1).add(state1);
                        }

                        if (!stateToNewGroup.containsKey(state2)) {
                            stateToNewGroup.put(state2, new HashSet<>());
                            stateToNewGroup.get(state2).add(state2);
                        }
                    }
                }));

                newPartitioning.addAll(new HashSet<>(stateToNewGroup.values()));
            }

            if (!finished) {
                partitions = newPartitioning;
                stateToGroup = stateToNewGroup;
            }
        }
    }

    @Override
    public MinCdfa build() {
        MinCdfa minCdfa = new MinCdfa();

        partitions.add(cdfa.getStates().stream().filter(State::isTerminal).collect(Collectors.toSet()));
        partitions.add(cdfa.getStates().stream().filter(state -> !state.isTerminal()).collect(Collectors.toSet()));

        partitions.forEach(partition -> partition.forEach(state -> stateToGroup.put(state, partition)));

        partition();

        List<Set<DfaState>> partitionsList = partitions.stream().toList();
        List<DfaState> partitionStatesList = new ArrayList<>();

        for (int i = 0; i < partitions.size(); ++i) {
            DfaState partitionState = new DfaState();
            partitionState.setName("m" + i);
            partitionStatesList.add(partitionState);
            partitionState.setTerminal(partitionsList.get(i).stream().anyMatch(DfaState::isTerminal));
        }

        for (int i = 0; i < partitions.size(); ++i) {
            DfaState partitionState = partitionStatesList.get(i);
            partitionsList.get(i).forEach(state -> {
                state.getΔ().forEach((sym, transition) -> {
                    int index = partitionsList.indexOf(stateToGroup.get(transition));
                    partitionState.addDfaState(partitionStatesList.get(index),sym);
                });
            });
        }

        minCdfa.getStates().addAll(partitionStatesList);
        minCdfa.setStartState(partitionStatesList.get(partitionsList.indexOf(stateToGroup.get(cdfa.getStartState()))));

        minCdfa.getStates().forEach(state -> {
            if (!state.isTerminal() && state.getΔ().values().stream().allMatch(adjacentState -> adjacentState.equals(state))) {
                state.setName("sink");
            }
        });

        return minCdfa;
    }
}
