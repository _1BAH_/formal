package atp.formal.automata.dfa;

import atp.formal.automata.states.DfaState;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class Cdfa extends Dfa {
    Set<Character> alphabet;

    public Cdfa() {
        super();
        startState = new DfaState();
        alphabet = new HashSet<>();
    }

    public Cdfa invert() {
        states.forEach(state -> state.setTerminal(!state.isTerminal()));
        return this;
    }

    @Override
    protected String getAutomataName() {
        return "complete deterministic finite automaton";
    }
}
