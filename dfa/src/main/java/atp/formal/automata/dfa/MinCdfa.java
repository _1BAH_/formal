package atp.formal.automata.dfa;

public class MinCdfa extends Cdfa {
    public MinCdfa() {
        super();
    }

    @Override
    protected String getAutomataName() {
        return "minimal complete deterministic finite automaton";
    }
}
