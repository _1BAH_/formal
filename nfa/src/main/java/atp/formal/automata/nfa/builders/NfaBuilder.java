package atp.formal.automata.nfa.builders;

import atp.formal.automata.builders.AutomatonBuilder;
import atp.formal.automata.builders.exceptions.NfaBuilderException;
import atp.formal.automata.states.NfaState;
import atp.formal.automata.nfa.EpsilonNfaWithSingleTerminalState;
import atp.formal.automata.nfa.Nfa;
import atp.formal.utils.Symbols;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Setter
public class NfaBuilder implements AutomatonBuilder {
    EpsilonNfaWithSingleTerminalState epsilonNfa;
    protected Set<NfaState> epsilonClosureCalculated = new HashSet<>();

    private void calculateEpsilonClosure() {
        for (NfaState state : epsilonNfa.getStates()) {
            transitiveClosureForState(state, state);
        }
    }

    private void transitiveClosureForState(NfaState state, NfaState start) {
        if (epsilonClosureCalculated.contains(state)) {
            return;
        }

        List<NfaState> epsilonTransitions = state.getΔTransitions(Symbols.EPSILON.getSymbol()).stream().toList();
        for (NfaState epsilonTransition : epsilonTransitions) {
            if (!epsilonTransition.equals(start)) {
                transitiveClosureForState(epsilonTransition, state);
            }
            epsilonTransition.getΔTransitions(Symbols.EPSILON.getSymbol())
                    .forEach(epsilonTransitiveState -> {
                        state.addΔTransition(Symbols.EPSILON.getSymbol(), epsilonTransitiveState);
                        state.setTerminal(state.isTerminal() || epsilonTransitiveState.isTerminal());
                    });

            state.setTerminal(state.isTerminal() || epsilonTransition.isTerminal());
        }

        epsilonClosureCalculated.add(state);
    }

    private void addNewTransitions() {
        for (NfaState state : epsilonNfa.getStates()) {
            addNewTransitionsToState(state);
        }
    }

    private static void addNewTransitionsToState(NfaState state) {
        Set<NfaState> epsilonMoves = state.getΔTransitions(Symbols.EPSILON.getSymbol());

        epsilonMoves.stream().filter(el -> el != state).forEach(move -> {
            move.getΔ().entrySet().stream().filter(
                    el -> el.getKey() != Symbols.EPSILON.getSymbol()
            ).forEach(transition -> {
                transition.getValue().forEach(transitiveState -> {
                    state.addΔTransition(transition.getKey(), transitiveState);

//                    if (transitiveState.isTerminal()) {
//                        state.setTerminal(true);
//                    }
                });
            });
        });
    }

    @Override
    public Nfa build() throws NfaBuilderException {
        calculateEpsilonClosure();
        addNewTransitions();

        epsilonNfa.setStates(new HashSet<>(epsilonNfa.getStates()));

//        for (NfaState state : epsilonNfa.getStates()) {
//            state.setTerminal(state.isTerminal() || state.getΔTransitions(Symbols.EPSILON.getSymbol()).stream().anyMatch(el -> el.equals(epsilonNfa.getFinalState())));
//        }

        Nfa nfa = new Nfa(epsilonNfa.getStates(), epsilonNfa.generateTransitionTable().getTransitionsTable());

        nfa.setStartState(epsilonNfa.getStartState());
        nfa.getStates().forEach(state -> {
            state.getΔ().remove(Symbols.EPSILON.getSymbol());
        });

        return nfa.clearEmptyStates();
    }
}
