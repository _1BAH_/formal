package atp.formal.automata.nfa;

import atp.formal.automata.SingleTerminalStateAutomaton;
import atp.formal.automata.states.NfaState;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@Getter
@Setter
public class EpsilonNfaWithSingleTerminalState extends SingleTerminalStateAutomaton {
    protected NfaState startState;
    private NfaState finalState;
    protected Set<NfaState> states;
    protected Map<NfaState, Map<Character, Set<NfaState>>> transitionsTable;

    public EpsilonNfaWithSingleTerminalState() {
        super();
        states = new HashSet<>();
        startState = new NfaState("0", false);
        finalState = new NfaState("1", false);
        states.add(this.getStartState());
        states.add(this.getFinalState());
    }

    public EpsilonNfaWithSingleTerminalState(Character sym) {
        this();
        startState.addΔTransition(sym, finalState);
    }

    public EpsilonNfaWithSingleTerminalState clearEmptyStates() {
        states.removeIf(state -> state.getΔ().entrySet().isEmpty() && state != getStartState() && state != getFinalState());
        return this;
    }

    public EpsilonNfaWithSingleTerminalState generateTransitionTable() {
        transitionsTable = new HashMap<>();

        for (NfaState state: states) {
            transitionsTable.put(state, new HashMap<>());
            for (var move: state.getΔ().entrySet()) {
                transitionsTable.get(state).put(move.getKey(), move.getValue());
            }
        }

        return this;
    }

    private String transitionTableToString() {
        List<String> lines = new ArrayList<>();

        for (var entry: transitionsTable.entrySet()) {
            for (var transition: entry.getValue().entrySet()) {
                for (var state: transition.getValue()) {
                    StringBuilder builder = new StringBuilder();

                    builder
                            .append(entry.getKey().getName())
                            .append(" ")
                            .append(transition.getKey())
                            .append(" ")
                            .append(state.getName());

                    lines.add(builder.toString());
                }
            }
        }

        return lines.stream().sorted().collect(Collectors.joining("\n"));
    }

    @Override
    protected String getAutomataName() {
        return "ε nondeterministic finite automata";
    }

    @Override
    protected String getTerminalStates() {
        StringBuilder builder = new StringBuilder();
        states.stream().filter(NfaState::isTerminal).map(NfaState::getName).sorted().forEach((stateName) -> {
            builder.append(stateName).append("\n");
        });

        return builder.toString();
    }

    @Override
    protected String getTransitionTable() {
        return generateTransitionTable().transitionTableToString();
    }
}
