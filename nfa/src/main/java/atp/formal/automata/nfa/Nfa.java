package atp.formal.automata.nfa;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;

import atp.formal.automata.states.NfaState;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Nfa extends EpsilonNfaWithSingleTerminalState {
    protected Map<NfaState, Boolean> marked = new HashMap<>();

    public Nfa(NfaState startState) {
        states = new HashSet<>();
        this.startState = startState;
        states.add(startState);
    }

    public Nfa(Set<NfaState> states, Map<NfaState, Map<Character, Set<NfaState>>> transitionTable) {
        super();
        this.states = states;
        this.transitionsTable = transitionTable;
        marked = new HashMap<>();
        states.forEach(state -> marked.put(state, false));
    }

    @Override
    public Nfa clearEmptyStates() {
        states.forEach(state -> state.getΔ().values().forEach(value -> value.forEach(e -> marked.put(e, true))));
        marked.put(getStartState(), true);
        marked.entrySet().stream().filter(entry -> !entry.getValue()).forEach(entry -> states.remove(entry.getKey()));
        return this;
    }

    @Override
    protected String getAutomataName() {
        return "nondeterministic finite automata without ε-transitions";
    }
}
