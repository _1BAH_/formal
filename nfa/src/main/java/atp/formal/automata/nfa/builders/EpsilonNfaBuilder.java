package atp.formal.automata.nfa.builders;

import atp.formal.automata.builders.exceptions.EpsilonNfaBuilderException;
import atp.formal.automata.nfa.EpsilonNfaWithSingleTerminalState;
import atp.formal.syntaxTree.nodes.Literal;
import atp.formal.syntaxTree.nodes.SyntaxTreeNode;
import atp.formal.utils.Symbols;
import atp.formal.automata.builders.AutomatonBuilder;
import atp.formal.automata.states.NfaState;
import atp.formal.syntaxTree.SyntaxTree;
import atp.formal.syntaxTree.nodes.binary.BinaryNode;
import atp.formal.syntaxTree.nodes.binary.Concatenation;
import atp.formal.syntaxTree.nodes.binary.Union;
import atp.formal.syntaxTree.nodes.unary.KleenePlus;
import atp.formal.syntaxTree.nodes.unary.KleeneStar;
import atp.formal.syntaxTree.nodes.unary.UnaryNode;
import lombok.Setter;

import java.util.Set;

@Setter
public class EpsilonNfaBuilder implements AutomatonBuilder {
    SyntaxTree syntaxTree;

    @Override
    public EpsilonNfaWithSingleTerminalState build() throws EpsilonNfaBuilderException {
        EpsilonNfaWithSingleTerminalState nfa = nodeToNfa(syntaxTree.getRoot()).clearEmptyStates();
        nfa.getFinalState().setTerminal(true);
        return nfa;
    }

    private static EpsilonNfaWithSingleTerminalState nodeToNfa(SyntaxTreeNode node) throws EpsilonNfaBuilderException {
        return switch (node) {
            case Literal l -> new EpsilonNfaWithSingleTerminalState(l.getValue());
            case UnaryNode unaryNode -> parseUnaryNode(unaryNode);
            case BinaryNode binaryNode -> parseBinaryNode(binaryNode);
            default -> throw new EpsilonNfaBuilderException("meet unknown vertex in syntax tree: " + node.getClass().getName());
        };
    }

    private static EpsilonNfaWithSingleTerminalState parseBinaryNode(BinaryNode node) throws EpsilonNfaBuilderException {
        EpsilonNfaWithSingleTerminalState left = nodeToNfa(node.getLeftElement());
        EpsilonNfaWithSingleTerminalState right = nodeToNfa(node.getRightElement());

        EpsilonNfaWithSingleTerminalState result = new EpsilonNfaWithSingleTerminalState();
        Set<NfaState> unionStates = result.getStates();

        for (NfaState state: left.getStates()) {
            state.setName("0" + state.getName());
            unionStates.add(state);
        }

        for (NfaState state: right.getStates()) {
            state.setName("1" + state.getName());
            unionStates.add(state);
        }

        switch (node) {
            case Union union -> {
                result.getStartState().addΔTransition(Symbols.EPSILON.getSymbol(), left.getStartState());
                result.getStartState().addΔTransition(Symbols.EPSILON.getSymbol(), right.getStartState());

                left.getFinalState().addΔTransition(Symbols.EPSILON.getSymbol(), result.getFinalState());
                right.getFinalState().addΔTransition(Symbols.EPSILON.getSymbol(), result.getFinalState());
            }
            case Concatenation concatenation -> {
                result.setStartState(left.getStartState());
                result.setFinalState(right.getFinalState());

                left.getFinalState().addΔTransition(Symbols.EPSILON.getSymbol(), right.getStartState());
            }
            default -> throw new EpsilonNfaBuilderException("meet unknown vertex in syntax tree: " + node.getClass().getName());
        }

        return result;
    }

    private static EpsilonNfaWithSingleTerminalState parseUnaryNode(UnaryNode node) throws EpsilonNfaBuilderException {
        EpsilonNfaWithSingleTerminalState nfa = nodeToNfa(node.getExpression());
        EpsilonNfaWithSingleTerminalState union = new EpsilonNfaWithSingleTerminalState();
        Set<NfaState> unionStates = union.getStates();

        for (NfaState state: nfa.getStates()) {
            state.setName("0" + state.getName());
            unionStates.add(state);
        }

        union.getStartState().addΔTransition(Symbols.EPSILON.getSymbol(), nfa.getStartState());
        nfa.getFinalState().addΔTransition(Symbols.EPSILON.getSymbol(), union.getFinalState());

        switch (node) {
            case KleeneStar star -> {
                nfa.getFinalState().addΔTransition(Symbols.EPSILON.getSymbol(), nfa.getStartState());
                union.getStartState().addΔTransition(Symbols.EPSILON.getSymbol(), union.getFinalState());
            }
            case KleenePlus plus -> {
                nfa.getFinalState().addΔTransition(Symbols.EPSILON.getSymbol(), nfa.getStartState());
            }
            default -> throw new EpsilonNfaBuilderException("meet unknown vertex in syntax tree: " + node.getClass().getName());
        }

        return union;
    }
}
