package atp.formal.automata.nfa.builders;

import atp.formal.parser.InfixParser;
import atp.formal.parser.exceptions.RegexpParserException;
import atp.formal.parser.exceptions.RegexpPreprocessorError;
import atp.formal.syntaxTree.SyntaxTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;

public class EpsilonNfaBuilderTests {
    @Test
    void buildTest() throws RegexpParserException, RegexpPreprocessorError {
        String regexp = "a+(b|c)*";
        String automaton = """
            ε nondeterministic finite automata
                        
            Start state:
            00
                        
            Final state:
            11
                        
            δ:
            00 ε 000
            000 a 001
            001 ε 000
            001 ε 01
            01 ε 10
            10 ε 100
            10 ε 11
            100 ε 1000
            100 ε 1010
            1000 b 1001
            1001 ε 101
            101 ε 100
            101 ε 11
            1010 c 1011
            1011 ε 101
            """;

        SyntaxTree tree = new InfixParser().setAlphabet(new HashSet<>(List.of('a', 'b', 'c'))).setRegexp(regexp).parse();
        Assertions.assertDoesNotThrow(() ->{
            Assertions.assertEquals(automaton, new EpsilonNfaBuilder().setSyntaxTree(tree).build().toString());
        });
    }

    @Test
    void trickyTest() throws RegexpParserException, RegexpPreprocessorError {
        String regexp = "(a*)*";
        String automaton = """
                ε nondeterministic finite automata
                            
                Start state:
                0
                            
                Final state:
                1
                            
                δ:
                0 ε 00
                0 ε 1
                00 ε 000
                00 ε 01
                000 a 001
                001 ε 000
                001 ε 01
                01 ε 00
                01 ε 1
                """;

        SyntaxTree tree = new InfixParser().setAlphabet(new HashSet<>(List.of('a', 'b', 'c'))).setRegexp(regexp).parse();
        Assertions.assertDoesNotThrow(() ->{
            Assertions.assertEquals(automaton, new EpsilonNfaBuilder().setSyntaxTree(tree).build().toString());
        });
    }
}
