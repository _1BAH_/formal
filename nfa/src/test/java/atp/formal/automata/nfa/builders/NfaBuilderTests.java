package atp.formal.automata.nfa.builders;

import atp.formal.automata.builders.exceptions.EpsilonNfaBuilderException;
import atp.formal.automata.nfa.EpsilonNfaWithSingleTerminalState;
import atp.formal.parser.InfixParser;
import atp.formal.parser.exceptions.RegexpParserException;
import atp.formal.parser.exceptions.RegexpPreprocessorError;
import atp.formal.syntaxTree.SyntaxTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;

public class NfaBuilderTests {
    @Test
    void buildTest() throws RegexpParserException, RegexpPreprocessorError, EpsilonNfaBuilderException {
        String regexp = "a+(b|c)*";
        String automaton = """
                nondeterministic finite automata without ε-transitions
                            
                Start state:
                00
                            
                Final state:
                001
                1001
                1011
                            
                δ:
                00 a 001
                001 a 001
                001 b 1001
                001 c 1011
                1001 b 1001
                1001 c 1011
                1011 b 1001
                1011 c 1011
                """;

        SyntaxTree tree = new InfixParser().setAlphabet(new HashSet<>(List.of('a', 'b', 'c'))).setRegexp(regexp).parse();
        EpsilonNfaWithSingleTerminalState epsilonNfa = new EpsilonNfaBuilder().setSyntaxTree(tree).build();
        Assertions.assertDoesNotThrow(() ->{
            Assertions.assertEquals(automaton, new NfaBuilder().setEpsilonNfa(epsilonNfa).build().toString());
        });
    }

    @Test
    void trickyTest() throws RegexpParserException, RegexpPreprocessorError, EpsilonNfaBuilderException {
        String regexp = "(a*)*";
        String automaton = """
                nondeterministic finite automata without ε-transitions
                                
                Start state:
                0
                                
                Final state:
                0
                001
                                
                δ:
                0 a 001
                001 a 001
                """;

        SyntaxTree tree = new InfixParser().setAlphabet(new HashSet<>(List.of('a', 'b', 'c'))).setRegexp(regexp).parse();
        EpsilonNfaWithSingleTerminalState epsilonNfa = new EpsilonNfaBuilder().setSyntaxTree(tree).build();
        Assertions.assertDoesNotThrow(() ->{
            Assertions.assertEquals(automaton, new NfaBuilder().setEpsilonNfa(epsilonNfa).build().toString());
        });
    }
}
