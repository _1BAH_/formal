package atp.formal.automata.nfa;

import atp.formal.automata.states.NfaState;
import atp.formal.utils.Symbols;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class NfaTests {
    private Nfa nfa;

    @BeforeEach
    void init() {
        nfa = new Nfa(new NfaState("0", false));
        NfaState finalState = new NfaState("1", true);
        nfa.states.add(finalState);
        nfa.startState.addΔTransition(Symbols.EPSILON.getSymbol(), finalState);
    }

    @Test
    void clearEmptyStatesTest() {
        var state = new NfaState("4", false);
        nfa.marked.put(state, false);
        nfa.states.add(state);

        Assertions.assertEquals(2, nfa.clearEmptyStates().states.size());
    }

    @Test
    void toStringTest() {
        Assertions.assertEquals(
                """
                        nondeterministic finite automata without ε-transitions
                        
                        Start state:
                        0
                                        
                        Final state:
                        1
                                        
                        δ:
                        0 ε 1
                        """,
                nfa.toString()
        );
    }
}
