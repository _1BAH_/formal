package atp.formal.automata.nfa;

import atp.formal.automata.states.NfaState;
import atp.formal.utils.Symbols;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EpsilonNfaTests {
    private EpsilonNfaWithSingleTerminalState epsilonNfa;

    @BeforeEach
    void init() {
        epsilonNfa = new EpsilonNfaWithSingleTerminalState();
    }

    @Test
    void emptyConstructorTest() {
        Assertions.assertEquals(2, epsilonNfa.states.size());
        Assertions.assertEquals(0, epsilonNfa.states.stream().filter(NfaState::isTerminal).count());
    }


    @Test
    void initialSymbolConstructorTest() {
        epsilonNfa = new EpsilonNfaWithSingleTerminalState(Symbols.EPSILON.getSymbol());
        Assertions.assertEquals(2, epsilonNfa.states.size());
        Assertions.assertEquals(0, epsilonNfa.states.stream().filter(NfaState::isTerminal).count());
        Assertions.assertEquals(1, epsilonNfa.getStartState().getΔTransitions(Symbols.EPSILON.getSymbol()).size());
    }

    @Test
    void clearEmptyStatesTest() {
        epsilonNfa.startState.addΔTransition(Symbols.DELTA.getSymbol(), new NfaState("4", false));
        epsilonNfa.states.add(epsilonNfa.startState.getΔTransitions(Symbols.DELTA.getSymbol()).iterator().next());

        Assertions.assertEquals(2, epsilonNfa.clearEmptyStates().states.size());
    }

    @Test
    void toStringTest() {
        epsilonNfa = new EpsilonNfaWithSingleTerminalState(Symbols.EPSILON.getSymbol());
        Assertions.assertEquals(
        """
                ε nondeterministic finite automata
                
                Start state:
                0
                                
                Final state:
                                
                δ:
                0 ε 1
                """,
                epsilonNfa.toString()
        );
    }
}
