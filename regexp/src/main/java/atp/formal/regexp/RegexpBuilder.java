package atp.formal.regexp;

import atp.formal.automata.regexp.Gdfa;
import atp.formal.automata.states.GdfaState;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Setter
public class RegexpBuilder {
    public Gdfa gdfa = new Gdfa();

    public String build() {
        while (gdfa.getStates().size() > 2) {
            gdfa.getStates().forEach(GdfaState::calculateTransitions);
            gdfa.getStates().forEach(GdfaState::uniteTransitions);

            gdfa.getStartState().getΔ().forEach((regexp, state) -> {
                var ref = new Object() {
                    String transition = regexp;
                };

                if (state.getName().equals("sink")) {
                    return;
                }

                if (state.getTransitions().containsKey(state)) {
                    ref.transition += "(" + state.getTransitions().get(state).iterator().next() + ")*";
                }

                state.getΔ().forEach((secondRegexp, secondState) -> {
                    if (secondState.equals(state)) {
                        return;
                    }

                    String transition = ref.transition + secondRegexp;

                    gdfa.getStartState().getTransitions().put(secondState, new HashSet<>(List.of(transition)));
                    gdfa.getStartState().uniteTransitions().calculateΔ();
                } );

                gdfa.getStartState().getTransitions().remove(state);
                gdfa.getStartState().uniteTransitions().calculateΔ();
                if (!state.equals(gdfa.getFinalState())) {
                    state.process();
                }
            });

            gdfa.setStates(gdfa.getStates().stream().filter(state -> !state.isProceed()).collect(Collectors.toSet()));
            gdfa.getStartState().uniteTransitions().calculateΔ();

        }

        return gdfa.getStartState().getTransitions().get(gdfa.getFinalState()).iterator().next();
    }
}
