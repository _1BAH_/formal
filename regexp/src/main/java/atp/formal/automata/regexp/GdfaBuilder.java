package atp.formal.automata.regexp;

import atp.formal.automata.builders.AutomatonBuilder;
import atp.formal.automata.dfa.Cdfa;
import atp.formal.automata.states.DfaState;
import atp.formal.automata.states.GdfaState;
import lombok.Setter;

@Setter
public class GdfaBuilder implements AutomatonBuilder {
    Cdfa cdfa = new Cdfa();

    @Override
    public Gdfa build() {
        Gdfa gdfa = new Gdfa();

        DfaState start = cdfa.getStartState();

        cdfa.getStates().stream().filter(state -> !state.getName().equals("sink")).forEach(state -> gdfa.addState(state, state.equals(start)));

        cdfa.getStates().stream().filter(state -> !state.getName().equals("sink")).forEach(state -> {
            var delta = gdfa.states.stream()
                    .filter(el -> el.getName().equals(state.getName()))
                    .map(GdfaState::getΔ).findAny().get();

            state.getΔ().forEach((sym, adjacentState) -> {
                if (!adjacentState.getName().equals("sink")) {
                    delta.put(String.valueOf(sym),
                            gdfa.states.stream()
                                    .filter(el -> el.getName().equals(adjacentState.getName())).findAny().get()
                    );
                }
            });
        });

        return gdfa;
    }
}
