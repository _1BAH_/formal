package atp.formal.automata.regexp;

import atp.formal.automata.Automaton;
import atp.formal.automata.states.DfaState;
import atp.formal.automata.states.GdfaState;
import atp.formal.utils.Symbols;
import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

@Getter
public class Gdfa extends Automaton {
    protected GdfaState startState;
    protected GdfaState finalState;
    protected Set<GdfaState> states;

    public Gdfa() {
        super();
        startState = new GdfaState("start", false);
        finalState = new GdfaState("final", true);
        states = new HashSet<>();
        states.add(startState);
        states.add(finalState);
    }

    public void addState(DfaState state, boolean isStart) {
        GdfaState gdfaState = new GdfaState(state.getName(), false);
        states.add(gdfaState);

        if (state.isTerminal()) {
            gdfaState.addΔTransition(String.valueOf(Symbols.EPSILON.getSymbol()), finalState);
        }

        if (isStart) {
            startState.addΔTransition(String.valueOf(Symbols.EPSILON.getSymbol()), gdfaState);
        }
    }

    public void setStates(Set<GdfaState> states) {
        this.states = states;
    }

    @Override
    public GdfaState getStartState() {
        return startState;
    }

    @Override
    protected String getAutomataName() {
        return "generalised deterministic finite automaton";
    }

    @Override
    protected String getTerminalStates() {
        return finalState.getName();
    }

    @Override
    protected String getTransitionTable() {
        return "";
    }
}
