package atp.formal.states;

import atp.formal.automata.states.DfaState;
import atp.formal.automata.states.NfaState;
import atp.formal.utils.Symbols;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DfaStateTests {
    private static DfaState state = new DfaState();
    private DfaState other;

    @BeforeEach
    void prepare() {
        other = new DfaState();
    }

    @Test
    void addTransitionTest() {
        state.addDfaState(other, Symbols.EPSILON.getSymbol());
        Assertions.assertSame(other, state.getΔ().get(Symbols.EPSILON.getSymbol()));
    }

    @Test
    void addTerminalNfaStatesTest() {
        NfaState nfaState = new NfaState("1", true);
        other.addNfaState(nfaState);

        Assertions.assertTrue(other.isTerminal());
        Assertions.assertTrue(other.getNfaStates().contains(nfaState));
    }

    @Test
    void addNonTerminalNfaStatesTest() {
        NfaState nfaState = new NfaState("1", false);
        other.addNfaState(nfaState);

        Assertions.assertFalse(other.isTerminal());
        Assertions.assertTrue(other.getNfaStates().contains(nfaState));
    }

    @Test
    void addSeveralNfaStatesNonTerminalTest() {
        NfaState nfaState1 = new NfaState("1", false);
        NfaState nfaState2 = new NfaState("3", false);
        other.addNfaState(nfaState1);
        other.addNfaState(nfaState2);

        Assertions.assertFalse(other.isTerminal());
        Assertions.assertTrue(other.getNfaStates().contains(nfaState1));
        Assertions.assertTrue(other.getNfaStates().contains(nfaState2));
    }

    @Test
    void addSeveralNfaStatesWithTerminalTest() {
        NfaState nfaState1 = new NfaState("1", false);
        NfaState nfaState2 = new NfaState("3", true);
        other.addNfaState(nfaState1);
        other.addNfaState(nfaState2);

        Assertions.assertTrue(other.isTerminal());
        Assertions.assertTrue(other.getNfaStates().contains(nfaState1));
        Assertions.assertTrue(other.getNfaStates().contains(nfaState2));
    }

    @Test
    void generateNameTest() {
        NfaState nfaState1 = new NfaState("1", false);
        NfaState nfaState2 = new NfaState("3", true);

        other.addNfaState(nfaState1);
        other.addNfaState(nfaState2);

        Assertions.assertEquals("1-3", other.calculateName().getName());
    }
}
