package atp.formal.states;

import atp.formal.automata.states.State;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StateTests {
    @Test
    void equalsTest() {
        State first = new State("1", false);
        State second = new State("1", true);
        Assertions.assertEquals(first, second);

        first.setName("2");
        Assertions.assertNotEquals(first, second);
    }
}
