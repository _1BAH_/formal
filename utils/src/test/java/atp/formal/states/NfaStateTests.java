package atp.formal.states;

import atp.formal.automata.states.NfaState;
import atp.formal.utils.Symbols;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;

public class NfaStateTests {
    private static NfaState state = new NfaState("1", false);
    private static NfaState other = new NfaState("2", false);
    private static HashSet<NfaState> transition = new HashSet<>(List.of(other));

    @BeforeAll
    static void init() {
        state.getΔ().put(Symbols.EPSILON.getSymbol(), transition);
    }

    @Test
    void getExistingΔTransitionsTest() {
        Assertions.assertSame(transition, state.getΔTransitions(Symbols.EPSILON.getSymbol()));
    }

    @Test
    void getNonExistingΔTransitionsTest() {
        Assertions.assertTrue(state.getΔTransitions(Symbols.DELTA.getSymbol()).isEmpty());
    }

    @Test
    void addΔTransitionsTest() {
        other.addΔTransition(Symbols.EPSILON.getSymbol(), state);
        Assertions.assertEquals(1, other.getΔ().get(Symbols.EPSILON.getSymbol()).size());
        Assertions.assertSame(state, other.getΔ().get(Symbols.EPSILON.getSymbol()).iterator().next());
    }

    @Test
    void findTransitionsTest() {
        Assertions.assertTrue(state.hasTransition(Symbols.EPSILON.getSymbol(), other));
        Assertions.assertFalse(state.hasTransition(Symbols.EPSILON.getSymbol(), state));
        Assertions.assertFalse(state.hasTransition(Symbols.DELTA.getSymbol(), other));
    }
}
