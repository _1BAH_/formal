package atp.formal.syntaxTree;

import atp.formal.syntaxTree.nodes.SyntaxTreeNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SyntaxTree {
    private SyntaxTreeNode root;
}
