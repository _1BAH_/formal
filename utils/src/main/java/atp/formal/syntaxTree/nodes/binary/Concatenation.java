package atp.formal.syntaxTree.nodes.binary;

import atp.formal.syntaxTree.nodes.SyntaxTreeNode;

public class Concatenation extends BinaryNode {
    public Concatenation(SyntaxTreeNode leftElement, SyntaxTreeNode rightElement) {
        super(rightElement, leftElement); /* ATTENTION!!! CHANGED POSITIONS */
    }

}
