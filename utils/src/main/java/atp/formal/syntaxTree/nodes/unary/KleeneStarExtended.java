package atp.formal.syntaxTree.nodes.unary;

import atp.formal.syntaxTree.nodes.SyntaxTreeNode;

public class KleeneStarExtended extends KleeneStar {

    public KleeneStarExtended(SyntaxTreeNode subNode) {
        super(subNode);
    }

    public KleeneStarExtended() {
        super();
    }
}
