package atp.formal.syntaxTree.nodes;

import atp.formal.utils.Symbols;
import lombok.Data;

@Data
public class Literal implements SyntaxTreeNode {
     private char value;

     public Literal(char value) {
          this.value = (value == '1') ? Symbols.EPSILON.getSymbol() : value;
     }
}
