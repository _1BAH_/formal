package atp.formal.syntaxTree.nodes.unary;

import atp.formal.syntaxTree.nodes.SyntaxTreeNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public abstract class UnaryNode implements SyntaxTreeNode {
    private SyntaxTreeNode expression;
}
