package atp.formal.syntaxTree.nodes.binary;

import atp.formal.syntaxTree.nodes.SyntaxTreeNode;

public class Union extends BinaryNode {
    public Union(SyntaxTreeNode leftElement, SyntaxTreeNode rightElement) {
        super(rightElement, leftElement); /* ATTENTION!!! CHANGED POSITIONS */
    }

}
