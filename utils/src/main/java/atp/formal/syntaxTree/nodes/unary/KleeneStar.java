package atp.formal.syntaxTree.nodes.unary;

import atp.formal.syntaxTree.nodes.SyntaxTreeNode;

public class KleeneStar extends UnaryNode {
    public KleeneStar(SyntaxTreeNode subNode) {
        super(subNode);
    }

    public KleeneStar() {
        super();
    }
}
