package atp.formal.syntaxTree.nodes.binary;

import atp.formal.syntaxTree.nodes.SyntaxTreeNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public abstract class BinaryNode implements SyntaxTreeNode {
    private SyntaxTreeNode leftElement;
    private SyntaxTreeNode rightElement;
}
