package atp.formal.syntaxTree.nodes.unary;

import atp.formal.syntaxTree.nodes.SyntaxTreeNode;

public class KleenePlus extends UnaryNode {

    public KleenePlus(SyntaxTreeNode subNode) {
        super(subNode);
    }
}
