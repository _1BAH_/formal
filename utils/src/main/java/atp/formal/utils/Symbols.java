package atp.formal.utils;

public enum Symbols {
    EPSILON('ε'),
    DELTA('δ');

    private final char symbol;

    Symbols(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }
}
