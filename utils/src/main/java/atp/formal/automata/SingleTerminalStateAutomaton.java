package atp.formal.automata;

import atp.formal.automata.states.State;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class SingleTerminalStateAutomaton extends Automaton {
    protected State finalState;

    public SingleTerminalStateAutomaton() {
        super();
        finalState = null;
    }
}
