package atp.formal.automata;

import atp.formal.automata.states.State;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class Automaton {
    public abstract State getStartState();
    protected abstract String getAutomataName();
    protected abstract String getTerminalStates();
    protected abstract String getTransitionTable();

    protected void toStringPreprocessor() {
        for (Method method: this.getClass().getDeclaredMethods()) {
            method.setAccessible(true);
            if (method.isAnnotationPresent(PreprocessToString.class)) {
                try {
                    method.invoke(this);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @Override
    public String toString() {
        toStringPreprocessor();

        return """
                %s
                
                Start state:
                %s
                
                Final state:
                %s
                δ:
                %s
                """.formatted(
                getAutomataName(),
                getStartState().getName(),
                getTerminalStates(),
                getTransitionTable()
        );
    }
}
