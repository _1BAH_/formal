package atp.formal.automata.builders.exceptions;

public class AutomatonBuilderException extends Exception {
    public AutomatonBuilderException(String automatonName, String message) {
        super("Error while building " + automatonName + ": " + message);
    }
}
