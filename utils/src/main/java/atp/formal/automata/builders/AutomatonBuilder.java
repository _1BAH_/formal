package atp.formal.automata.builders;

import atp.formal.automata.Automaton;
import atp.formal.automata.builders.exceptions.AutomatonBuilderException;

public interface AutomatonBuilder {
    Automaton build() throws AutomatonBuilderException;
}
