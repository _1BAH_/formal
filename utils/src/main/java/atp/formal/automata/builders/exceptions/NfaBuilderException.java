package atp.formal.automata.builders.exceptions;

public class NfaBuilderException extends AutomatonBuilderException {
    public NfaBuilderException(String message) {
        super("nondeterministic finite automaton", message);
    }
}
