package atp.formal.automata.builders.exceptions;

public class EpsilonNfaBuilderException extends AutomatonBuilderException {
    public EpsilonNfaBuilderException(String message) {
        super("epsilon nondeterministic finite automaton", message);
    }
}
