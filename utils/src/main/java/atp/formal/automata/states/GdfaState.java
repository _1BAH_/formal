package atp.formal.automata.states;

import lombok.Getter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Getter
public class GdfaState extends State {
    private Map<String, GdfaState> δ = new HashMap<>();
    private Map<GdfaState, Set<String>> transitions = new HashMap<>();
    private boolean proceed = false;

    public GdfaState(String name, boolean isTerminal) {
        super(name, isTerminal);
    }

    public void calculateΔ() {
        this.δ.clear();

        transitions.forEach((transition, regexp) -> {
            this.δ.put(regexp.iterator().next(), transition);
        });

    }
    public void calculateTransitions() {
        transitions.clear();
        this.δ.forEach((regexp, transition) -> {
            if (transitions.containsKey(transition)) {
                transitions.get(transition).add(regexp);
            } else {
                transitions.put(transition, new HashSet<>(List.of(regexp)));
            }
        });

    }

    public void addΔTransition(String regexp, GdfaState state) {
        this.δ.put(regexp, state);
    }

    public GdfaState uniteTransitions() {
        transitions.forEach((transition, regexps) -> {
            regexps.forEach(regexp -> this.δ.remove(regexp));
            this.δ.put("(" + String.join("|", regexps) + ")", transition);
        });

        calculateTransitions();

        return this;
    }

    public void process() {
        proceed = true;
    }
//    private
}
