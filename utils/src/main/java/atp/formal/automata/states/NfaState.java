package atp.formal.automata.states;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NfaState extends State {
    private Map<Character, Set<NfaState>> δ;

    public NfaState(String name, boolean isTerminal) {
        super(name, isTerminal);
        this.δ = new HashMap<>();
    }

    public void addΔTransition(Character sym, NfaState state) {
        if (!δ.containsKey(sym)) {
            δ.put(sym, new HashSet<>());
        }

        δ.get(sym).add(state);
    }

    public Set<NfaState> getΔTransitions(Character sym) {
        return δ.getOrDefault(sym, Collections.emptySet());
    }

    public boolean hasTransition(char sym, State state) {
        return getΔTransitions(sym).contains(state);
    }
}
