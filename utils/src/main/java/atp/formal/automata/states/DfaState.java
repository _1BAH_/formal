package atp.formal.automata.states;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class DfaState extends State {
    private Map<Character, DfaState> δ;
    private Set<NfaState> nfaStates;
    private Map<Character, Set<NfaState>> nfaTransitions;

    public DfaState() {
        super("", false);
        δ = new HashMap<>();
        nfaTransitions = new HashMap<>();
        nfaStates = new HashSet<>();
    }

    public DfaState addDfaState(DfaState state, Character transition) {
        δ.put(transition, state);
        return this;
    }

    public DfaState addNfaState(NfaState state) {
        nfaStates.add(state);
        state.getΔ().forEach((sym, adjacentVertices) -> {
            if (!nfaTransitions.containsKey(sym)) {
                nfaTransitions.put(sym, new HashSet<>());
            }
            adjacentVertices.forEach(nfaTransitions.get(sym)::add);
        });
        isTerminal = isTerminal || state.isTerminal;
        return this;
    }

    public DfaState calculateName() {
        name = nfaStates.stream().map(state -> state.name).collect(Collectors.joining("-"));
        return this;
    }


}
