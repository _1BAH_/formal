package atp.formal.automata.states;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class State {
    protected String name;
    protected boolean isTerminal;

    @Override
    public boolean equals(Object o) {
        return switch (o) {
            case State other -> name != null && name.equals(other.name);
            default -> false;
        };
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
