package atp.formal.parser;

import atp.formal.parser.exceptions.RegexpParserException;
import atp.formal.syntaxTree.SyntaxTree;
import atp.formal.syntaxTree.nodes.Literal;
import atp.formal.syntaxTree.nodes.binary.Concatenation;
import atp.formal.syntaxTree.nodes.binary.Union;
import atp.formal.syntaxTree.nodes.unary.KleenePlus;
import atp.formal.syntaxTree.nodes.unary.KleeneStar;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class InfixParserTests {
    Set<Character> alphabet = new HashSet<>(Arrays.asList('a', 'b', 'c'));

    @Test
    void kleeneStarTest() {
        String regexp = "a*";

        Assertions.assertDoesNotThrow(() -> {
            SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
            Assertions.assertEquals(KleeneStar.class, tree.getRoot().getClass());
            Assertions.assertEquals(Literal.class, ((KleeneStar)tree.getRoot()).getExpression().getClass());
            Assertions.assertEquals('a', ((Literal)((KleeneStar)tree.getRoot()).getExpression()).getValue());
        });
    }


    @Test
    void kleenePlusTest() {
        String regexp = "a+";

        Assertions.assertDoesNotThrow(() -> {
            SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
            Assertions.assertEquals(KleenePlus.class, tree.getRoot().getClass());
            Assertions.assertEquals(Literal.class, ((KleenePlus)tree.getRoot()).getExpression().getClass());
            Assertions.assertEquals('a', ((Literal)((KleenePlus)tree.getRoot()).getExpression()).getValue());
        });
    }

    @Test
    void unionTest() {
        String regexp = "(a | b)";

        Assertions.assertDoesNotThrow(() -> {
            SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
            Assertions.assertEquals(Union.class, tree.getRoot().getClass());

            Assertions.assertEquals(Literal.class, ((Union)tree.getRoot()).getLeftElement().getClass());
            Assertions.assertEquals('a', ((Literal)((Union)tree.getRoot()).getLeftElement()).getValue());

            Assertions.assertEquals(Literal.class, ((Union)tree.getRoot()).getRightElement().getClass());
            Assertions.assertEquals('b', ((Literal)((Union)tree.getRoot()).getRightElement()).getValue());
        });
    }

    @Test
    void concatenationTest() {
        String regexp = "ab";

        Assertions.assertDoesNotThrow(() -> {
            SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
            Assertions.assertEquals(Concatenation.class, tree.getRoot().getClass());

            Assertions.assertEquals(Literal.class, ((Concatenation)tree.getRoot()).getLeftElement().getClass());
            Assertions.assertEquals('a', ((Literal)((Concatenation)tree.getRoot()).getLeftElement()).getValue());

            Assertions.assertEquals(Literal.class, ((Concatenation)tree.getRoot()).getRightElement().getClass());
            Assertions.assertEquals('b', ((Literal)((Concatenation)tree.getRoot()).getRightElement()).getValue());
        });
    }

    @Test
    void exceptionTest() {
        String regexp = "a(b";

        Assertions.assertThrows(RegexpParserException.class, () -> {
            SyntaxTree tree = new InfixParser().setAlphabet(alphabet).setRegexp(regexp).parse();
        });
    }
}
