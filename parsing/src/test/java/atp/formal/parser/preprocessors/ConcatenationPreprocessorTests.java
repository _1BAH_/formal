package atp.formal.parser.preprocessors;

import atp.formal.parser.exceptions.RegexpPreprocessorError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ConcatenationPreprocessorTests extends PreprocessorTests {
    @BeforeEach
    void init() {
        preprocessor = new ConcatenationPreprocessor().setAlphabet(alphabet);
    }

    @Test
    void oneSymbolTest() {
        String regexp = "a";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals(regexp, preprocessor.setRegexp(regexp).process());
        });
    }

    @Test
    void twoSymbolTest() {
        String regexp = "ab";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("a.b", preprocessor.setRegexp(regexp).process());
        });
    }

    @Test
    void ParenthesesTest() {
        String regexp = "(a)(b)";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("(a).(b)", preprocessor.setRegexp(regexp).process());
        });
    }

    @Test
    void UnionTest() {
        String regexp = "(a|b)";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals(regexp, preprocessor.setRegexp(regexp).process());
        });
    }

    @Test
    void kleeneTest() {
        String regexp = "ca*cb+c";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("c.a*.c.b+.c", preprocessor.setRegexp(regexp).process());
        });
    }

    @Test
    void complexTest() {
        String regexp = "a(c(a|b)*)+(b*|ca)";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("a.(c.(a|b)*)+.(b*|c.a)", preprocessor.setRegexp(regexp).process());
        });
    }

    @Test
    void nullAlphabet() {
        String regexp = "a";

        Assertions.assertThrows(RegexpPreprocessorError.class, () -> {
            new ConcatenationPreprocessor().setRegexp(regexp).process();
        });
    }

    @Test
    void nullRegexp() {
        Assertions.assertThrows(RegexpPreprocessorError.class, () -> {
            preprocessor.process();
        });
    }
}
