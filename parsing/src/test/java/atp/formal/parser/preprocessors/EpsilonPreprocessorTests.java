package atp.formal.parser.preprocessors;

import atp.formal.parser.exceptions.RegexpPreprocessorError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EpsilonPreprocessorTests extends PreprocessorTests {
    @BeforeEach
    void init() {
        preprocessor = new EpsilonPreprocessor().setAlphabet(alphabet);
    }

    @Test
    void oneSymbolTest() {
        String regexp = "1";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals(regexp, preprocessor.setRegexp(regexp).process());
        });
    }

    @Test
    void epsilonNTest() {
        String regexp1 = "11";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("1", preprocessor.setRegexp(regexp1).process());
        });

        String regexp2 = "1111111111111111111111111111111111111111111111111";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("1", preprocessor.setRegexp(regexp2).process());
        });
    }

    @Test
    void kleeneTest() {
        String regexp1 = "1*";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("1", preprocessor.setRegexp(regexp1).process());
        });

        String regexp2 = "1+";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("1", preprocessor.setRegexp(regexp2).process());
        });
    }

    @Test
    void kleeneParenthesesTest() {
        String regexp1 = "(1)*";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("1", preprocessor.setRegexp(regexp1).process());
        });

        String regexp2 = "(1)+";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("1", preprocessor.setRegexp(regexp2).process());
        });
    }

    @Test
    void unionTest() {
        String regexp = "(1|a)";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals(regexp, preprocessor.setRegexp(regexp).process());
        });
    }

    @Test
    void concatenationTest() {
        String regexp1 = "1a";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("a", preprocessor.setRegexp(regexp1).process());
        });

        String regexp2 = "a1";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("a", preprocessor.setRegexp(regexp2).process());
        });

        String regexp3 = "a1a";

        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals("aa", preprocessor.setRegexp(regexp3).process());
        });
    }

    @Test
    void nullAlphabet() {
        String regexp = "a";

        Assertions.assertThrows(RegexpPreprocessorError.class, () -> {
            new EpsilonPreprocessor().setRegexp(regexp).process();
        });
    }

    @Test
    void nullRegexp() {
        Assertions.assertThrows(RegexpPreprocessorError.class, () -> {
            preprocessor.process();
        });
    }
}
