package atp.formal.parser.preprocessors;

import atp.formal.parser.exceptions.RegexpPreprocessorError;

import java.util.Set;

public class EpsilonPreprocessor implements RegexpPreprocessor {
    private String regexp;
    private Set<Character> alphabet;

    @Override
    public EpsilonPreprocessor setRegexp(String regexp) {
        this.regexp = regexp;
        return this;
    }

    @Override
    public EpsilonPreprocessor setAlphabet(Set<Character> alphabet) {
        this.alphabet = alphabet;
        return this;
    }

    private void reduceEpsilons() {
        while (
                regexp.contains("11")
                        || regexp.contains("1|1")
                        || regexp.contains("1*")
                        || regexp.contains("1+")
                        || regexp.contains("(1)+")
                        || regexp.contains("(1)*")
        ) {
            regexp = regexp.replaceAll("11", "1");
            regexp = regexp.replaceAll("1[|]1", "1");
            regexp = regexp.replaceAll("1[*]", "1");
            regexp = regexp.replaceAll("1[+]", "1");
            regexp = regexp.replaceAll("[(]1[)][+]", "1");
            regexp = regexp.replaceAll("[(]1[)][*]", "1");
        }
    }

    @Override
    public String process() throws RegexpPreprocessorError {
        if (alphabet == null) {
            throw new RegexpPreprocessorError("Alphabet is null");
        }

        if (regexp == null) {
            throw new RegexpPreprocessorError("Regexp is null");
        }

        reduceEpsilons();

        if (regexp.charAt(0) == '1' && regexp.length() > 1) {
            regexp = regexp.substring(1);
        }
        if (regexp.charAt(regexp.length() - 1) == '1' && regexp.length() > 1) {
            regexp = regexp.substring(0, regexp.length() - 1);
        }

        if (regexp.contains(")1")) {
            regexp = regexp.replaceAll("[)]1", ")");
        }

        for (char sym: alphabet) {
            regexp = regexp.replaceAll(sym + "1", String.valueOf(sym));
            regexp = regexp.replaceAll("1" + sym, String.valueOf(sym));
        }

        return regexp;
    }
}
