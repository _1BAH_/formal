package atp.formal.parser.preprocessors;

import atp.formal.parser.exceptions.RegexpPreprocessorError;

import java.util.Set;

public interface RegexpPreprocessor {
    String process() throws RegexpPreprocessorError;
    RegexpPreprocessor setRegexp(String regexp);
    RegexpPreprocessor setAlphabet(Set<Character> alphabet);
}
