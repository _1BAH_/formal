package atp.formal.parser.preprocessors;

import atp.formal.parser.exceptions.RegexpPreprocessorError;

import java.util.Set;

public class ConcatenationPreprocessor implements RegexpPreprocessor {
    private String regexp;
    private Set<Character> alphabet;

    @Override
    public RegexpPreprocessor setRegexp(String regexp) {
        this.regexp = regexp;
        return this;
    }

    @Override
    public RegexpPreprocessor setAlphabet(Set<Character> alphabet) {
        this.alphabet = alphabet;
        return this;
    }

    @Override
    public String process() throws RegexpPreprocessorError {
        if (alphabet == null) {
            throw new RegexpPreprocessorError("Alphabet is null");
        }

        if (regexp == null) {
            throw new RegexpPreprocessorError("Regexp is null");
        }

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < regexp.length() - 1; ++i) {
            switch (regexp.charAt(i)) {
                case '|', '(' -> {
                    builder.append(regexp.charAt(i));
                }
                default -> {
                    if (isLiteral(regexp.charAt(i + 1)) || regexp.charAt(i + 1) == '(') {
                        builder.append(regexp.charAt(i));
                        builder.append('.');
                    } else {
                        builder.append(regexp.charAt(i));
                    }
                }
            }
        }

        builder.append(regexp.charAt(regexp.length() - 1));
        return builder.toString();
    }

    private boolean isLiteral(char sym) {
        return alphabet.stream().anyMatch(el -> el.equals(sym));
    }
}
