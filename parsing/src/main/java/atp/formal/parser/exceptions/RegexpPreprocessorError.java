package atp.formal.parser.exceptions;

public class RegexpPreprocessorError extends Exception {
    public RegexpPreprocessorError(String message) {
        super("Error while preprocessing regexp: " + message);
    }
}
