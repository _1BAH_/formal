package atp.formal.parser.exceptions;

public class RegexpParserException extends Exception {
    public RegexpParserException(String message) {
        super("Error while parsing regexp: " + message);
    }
}
