package atp.formal.parser;

import atp.formal.parser.exceptions.RegexpParserException;
import atp.formal.parser.exceptions.RegexpPreprocessorError;
import atp.formal.parser.preprocessors.ConcatenationPreprocessor;
import atp.formal.parser.preprocessors.EpsilonPreprocessor;
import atp.formal.parser.preprocessors.RegexpPreprocessor;
import atp.formal.syntaxTree.SyntaxTree;
import atp.formal.syntaxTree.nodes.Literal;
import atp.formal.syntaxTree.nodes.SyntaxTreeNode;
import atp.formal.syntaxTree.nodes.binary.Concatenation;
import atp.formal.syntaxTree.nodes.binary.Union;
import atp.formal.syntaxTree.nodes.unary.KleenePlus;
import atp.formal.syntaxTree.nodes.unary.KleeneStar;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Stream;

public class InfixParser {
    private SyntaxTree tree;
    private char[] regexp;
    private String rawRegexp;
    private Stack<Character> operators;
    private Stack<SyntaxTreeNode> nodes;
    private List<RegexpPreprocessor> preprocessors;
    private Set<Character> alphabet;

    public InfixParser() {
        preprocessors = Arrays.asList(new EpsilonPreprocessor(), new ConcatenationPreprocessor());
        nodes = new Stack<>();
        operators = new Stack<>();
    }

    public InfixParser setRegexp(String regexp) {
        rawRegexp = regexp.replaceAll("\\s", "");
        return this;
    }

    public InfixParser setAlphabet(Set<Character> alphabet) {
        this.alphabet = alphabet;
        return this;
    }

    public SyntaxTree parse() throws RegexpPreprocessorError, RegexpParserException {
        for (RegexpPreprocessor preprocessor : preprocessors) {
            rawRegexp = preprocessor.setAlphabet(alphabet).setRegexp(rawRegexp).process();
        }
        regexp = rawRegexp.toCharArray();
        buildTree();
        return tree;
    }

    private void buildTree() throws RegexpParserException {
        for (char sym : regexp) {
            if (isLiteral(sym)) {
                nodes.push(new Literal(sym));
                continue;
            }

            if (operators.empty() || sym == '(') {
                operators.push(sym);
                continue;
            }

            if (sym == ')') {
                while (operators.get(operators.size() - 1) != '(') {
                    proceedOperator();
                }

                operators.pop();
                continue;
            }

            while (!operators.empty() && compareOrder(sym, operators.get(operators.size() - 1))) {
                proceedOperator();
            }

            operators.push(sym);
        }

        while (!operators.empty()) {
            proceedOperator();
        }

        tree = new SyntaxTree(nodes.pop());
    }

    private void proceedOperator() throws RegexpParserException {
        char operator = operators.pop();

        switch (operator) {
            case '*' -> {
                SyntaxTreeNode node = new KleeneStar(nodes.pop());
                nodes.push(node);
            }
            case '+' -> {
                SyntaxTreeNode node = new KleenePlus(nodes.pop());
                nodes.push(node);
            }
            case '|' -> {
                SyntaxTreeNode node = new Union(nodes.pop(), nodes.pop());
                nodes.push(node);
            }
            case '.' -> {
                SyntaxTreeNode node = new Concatenation(nodes.pop(), nodes.pop());
                nodes.push(node);
            }
            default -> throw new RegexpParserException("Unknown operator %c was met during parsing".formatted(operator));
        }
    }

    private boolean compareOrder(char first, char second) {
        if (first == second) {
            return true;
        }

        if (first == '*' || first == '+') {
            return false;
        }

        if (second == '*' || second == '+') {
            return true;
        }

        if (first == '.') {
            return false;
        }

        if (second == '.') {
            return true;
        }

        return first != '|';
    }

    private static boolean isLiteral(char sym) {
        return Stream.of('+', '|', '*', '.', '(', ')').noneMatch(el -> el == sym);
    }
}
